<?php
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');

include("Slim/Slim.php");

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app->response->headers->set('Content-Type', 'application/json');


$app->get('/',function() use ($app){

  $app->response->setBody(json_encode('API Funcionando'));
});


$app->get('/PagoOxxo',function() use ($app){
    $nombreCliente = $app->request->get('Nombre');
    $correoCliente = $app->request->get('Correo');
    $cantidadPago = $app->request->get('Cantidad');
    $telefonoCliente = $app->request->get('Telefono');


    require_once("conekta-php/lib/Conekta.php");
    \Conekta\Conekta::setApiKey("key_jwQ2NqGwGJ4gLMEPr9pbpA");
    \Conekta\Conekta::setApiVersion("2.0.0");
    \Conekta\Conekta::setLocale('es');

    try{
        $order = \Conekta\Order::create(
          array(
            "line_items" => array(
              array(
                "name" => "Recarga Saldo Yabu",
                "unit_price" => $cantidadPago,
                "quantity" => 1
              )//first line_item
            ),
            "currency" => "MXN",
            "customer_info" => array(
              "name" => $nombreCliente,
              "email" => $correoCliente,
              "phone" => "+52".$telefonoCliente
            ), //customer_info
            "charges" => array(
                array(
                    "payment_method" => array(
                      "type" => "oxxo_cash"
                    )//payment_method
                ) //first charge
            ) //charges
          )//order
        );

      } catch (\Conekta\ParameterValidationError $error){
        echo $error->getMessage();
      } catch (\Conekta\Handler $error){
        echo $error->getMessage();
      }

    $app->response->setBody(json_encode($order));
  });



$app->run();
?>
